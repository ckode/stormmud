#  StormMUD - A multiplayer combat game - http://stormmud.david-c-brown.com
#  Copyright (C) 2009, 2010 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import smLog

import sys

def UnrecoverableError():
    """Called when an unrecoverable error occurs.  Forces exiting of the server"""
    
    smLog.logger.Logit("An unrecoverable error occurred. Exiting...")
    sys.exit(1)
    

def UnspecifiedErrorExit():
    """Called when an exception happens that wasn't caught with a known exception.
       Currently it then calls UnrecoverableError that shutsdown the server.  This
       may not be what we want."""
    
    smLog.logger.Logit( "Unexpected error:", sys.exc_info()[0] )
    UnrecoverableError()
