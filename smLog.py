#  StormMUD - A multiplayer combat game - http://stormmud.david-c-brown.com
#  Copyright (C) 2009, 2010 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


#import smConfig
from smDefines import INFO, DEBUG, WARN, ERROR, FILE, CONSOLE

from time import strftime, localtime

logger = None

class smLogger:
    """
    StormMUD Logger class for logging info, errors and warnings, etc
    """

    def __init__(self):
        """
        Initialize the logger
        """
        # Moved import here to avoid circular importing :/
        from smConfig import GameConfig, CONSOLE, FILE

        self.Logfile = GameConfig.Logfile
        self.info  = {FILE: GameConfig.LogInfo[FILE], CONSOLE: GameConfig.LogInfo[CONSOLE] }
        self.debug = {FILE: GameConfig.LogDebug[FILE], CONSOLE: GameConfig.LogDebug[CONSOLE] }
        self.warn  = {FILE: GameConfig.LogWarn[FILE], CONSOLE: GameConfig.LogWarn[CONSOLE] }
        self.error = {FILE: GameConfig.LogError[FILE], CONSOLE: GameConfig.LogError[CONSOLE] }


    #=================================================
    # smLogger -> Logit()
    #=================================================
    def Logit(self, LogType, LogData):
        """
        Write information to the log file and/or console"
        """
        def WriteLog(where):
            if where[FILE]:
                try:
                    f = open(self.Logfile, 'a')
                    f.write("%s %s %s%s" % (cTime, lType, LogData, "\n") )
                    print "Finished writing log"
                except IOError as errstr:
                    print "Failed to log to %s: %s" % (self.Logfile, errstr)
                except Exception as errstr:
                    print "Unspecified error occured: %s" % (errstr)
                finally:
                    f.close()
            if where[CONSOLE]:
                print "%s %s %s" % (cTime, lType, LogData)

        cTime = strftime("%b %d %Y %H:%M:%S", localtime())

        if LogType == INFO:
            lType = 'INFO'
            WriteLog(self.info)
        elif LogType == DEBUG:
            lType = 'DEBUG'
            WriteLog(self.debug)
        elif LogType == WARN:
            lType = 'WARN'
            WriteLog(self.warn)
        elif LogType == ERROR:
            lType = 'ERROR'
            WriteLog(self.error)

