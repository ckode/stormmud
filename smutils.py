#  StormMUD - A multiplayer combat game - http://stormmud.david-c-brown.com
#  Copyright (C) 2009, 2010 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import string



#===============================================
# FindItemName()
#===============================================
def FindItemName(itemName, searchText):
    """
    FindItemname(itemName, searchText)
    Splits item name into a list if multiple words
    then searches each word to see if it matches
    searchText.  Returns true or false
    
    """
    
    names = itemName.split()
    if [ name for name in names if name.startswith(searchText) ]:
        return True
    else:
        return False

