#  StormMUD - A multiplayer combat game - http://stormmud.david-c-brown.com
#  Copyright (C) 2009, 2010 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


class NPC:
    """The NPC class.  Class for non-player characters. (monsters)"""
    
    def __init__(self):
        """NPC -> __init__(): Initialize the NPC class when created"""
        
        # NPC specifics
        self.name                 = "" # Also serves as the players player ID
        self.prename              = ""
        self.description          = ""
        self.Attks                = {1: None, 2: None, 3: None, 4: None, 5: None}
        self.alignment            = 0
        
        # NPC Stats
        self.HPs                  = 0
        self.MaxHPs               = 0
        self.offense              = 0
        self.defense              = 0
        self.strength             = 0
        self.agility              = 0
        self.intellect            = 0
        self.wisdom               = 0
        self.consitution          = 0
        self.charisma             = 0
        self.AttackSpeed          = 0
        self.preception           = 0
        self.stealth              = 0
        self.SpellCasting         = 0
        self.MagicRes             = 0
        self.SavingsThrow         = 0
        self.ArmorClass           = 0
        self.experience           = 0
        self.ResistHot            = 0
        self.ResistCold           = 0
        self.ResistAir            = 0
        self.ResistLightning      = 0
        self.ResistEarth          = 0
        self.ResistPsionics       = 0
        self.AttackSpeed          = 0
        self.CriticalChance       = 0
        self.BackstabBonus        = 0

               
        # Player flags: True or False only
        self.sneaking             = False
        self.paralyzed            = False
        self.held                 = False
        self.blind                = False
        self.BriefDesc            = False
        self.IsBackstabbing       = False
        self.resting              = False
        
        # Container attributes
        self.spells               = {}
        self.items                = [] # See attribute document
        self.money                = {}
