#  StormMUD - A multiplayer combat game - http://stormmud.david-c-brown.com
#  Copyright (C) 2009, 2010 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import string, re

import smDefines, smCommands
from smDefines import NORTH, NE, EAST, SE, SOUTH, SW, WEST, NW, UP, DOWN







def GameParser(player, line):
    """GameParser():  Primary game parser"""
       
    # Clean players input
    line = CleanPlayerInput(line)
    
    if line == "":
        smCommands.LookRoom(player, player.room)
        return
    
    if player.moving:
        player.AddCmdToStack(line)
        return
        
    
    # Command list.
    commands = { '/quit':            smCommands.Quit,
                 'north':            smCommands.CallMovePlayer,
                 'ne':               smCommands.CallMovePlayer,
                 'northeast':        smCommands.CallMovePlayer,
                 'east':             smCommands.CallMovePlayer,
                 'se':               smCommands.CallMovePlayer,
                 'southeast':        smCommands.CallMovePlayer,
                 'south':            smCommands.CallMovePlayer,
                 'sw':               smCommands.CallMovePlayer,
                 'southwest':        smCommands.CallMovePlayer,
                 'west':             smCommands.CallMovePlayer,
                 'nw':               smCommands.CallMovePlayer,
                 'northwest':        smCommands.CallMovePlayer,      
                 'up':               smCommands.CallMovePlayer,
                 'down':             smCommands.CallMovePlayer,
                 'rest':             smCommands.Rest
                 
               }
 
    
    cmd = line.split()
    
    # Compile search string
    cmdstr = re.compile(re.escape(cmd[0].lower()))
    
    # Cycle through the commands
    for each in commands.keys():
        if cmdstr.match(each):
            if each == "/quit" and len(cmd[0]) > 1:
                commands[each](player)
                return
            # Directional movements.
            elif each == "north" and len(cmd) == 1 and len(cmd[0]) != 2:
                commands[each](player, NORTH)
                return
            elif (each == "ne" and len(cmd) == 1 and len(cmd[0]) == 2):
                commands['northeast'](player, NE)
                return
            elif (each == "northeast" and len(cmd) == 1 and len(cmd[0]) > 5 ):
                commands['northeast'](player, NE)
                return
            elif each == "east" and len(cmd) == 1 and len(cmd[0]) != 2:
                commands[each](player, EAST)
                return
            elif (each == "se" and len(cmd) == 1 and len(cmd[0]) == 2): 
                commands['southeast'](player, SE)
                return
            elif (each == "southeast" and len(cmd) == 1 and len(cmd[0]) > 5):
                commands['southeast'](player, SE)
                return
            elif each == "south" and len(cmd) == 1 and len(cmd[0]) != 2:
                commands[each](player, SOUTH)
                return
            elif (each == "sw" and len(cmd) == 1 and len(cmd[0]) == 2):
                commands['southwest'](player, SW)
                return
            elif (each == "southwest" and len(cmd) == 1 and len(cmd[0]) > 5 ):
                commands['southwest'](player, SW)
                return
            elif each == "west" and len(cmd) == 1 and len(cmd[0]) != 2:
                commands[each](player, WEST)
                return
            elif (each == "nw" and len(cmd) == 1 and len(cmd[0]) == 2):
                commands['northwest'](player, NW)
                return
            elif (each == "northwest" and len(cmd) == 1 and len(cmd[0]) != 2):
                commands['northwest'](player, NW)
                return
            elif each == "up" and len(cmd) == 1 and len(cmd[0]) != 2:
                commands[each](player, UP)
                return
            elif each == "down" and len(cmd) == 1 and len(cmd[0]) != 2:
                commands[each](player, DOWN)
                return
            elif each == "rest" and len(cmd) == 1 and len(cmd[0]) == 4:
                commands[each](player)
                return                
                
    smCommands.Say(player, line)
    
    
    
    
    
    
    
    
    
    
    
#############################################################
# CleanPlayerInput()
#
# Clean players input by removing all unprintable characters
# and properly applying deletes by backspacing
#############################################################
def CleanPlayerInput(line):
    """CleanPlayerInput(): Removes unprintable characters from the
       players input and deletes characters if the player used the
       backspace key"""
    
    #Delete characters before backspaces
    pos = 0
    lineSize = len(line)
    newline = ""
    for character in line:
        if character == chr(0x08):
            newline = newline[:-1]
        else:
            newline += character
    # Remove all unprintable characters
    line = filter(lambda x: x in string.printable, newline)
    return line