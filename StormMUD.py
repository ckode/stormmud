#  StormMUD - A multiplayer combat game - http://stormmud.david-c-brown.com
#  Copyright (C) 2009, 2010 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Twisted imports
from twisted.internet.protocol import ServerFactory
from twisted.internet import reactor
from twisted.conch.telnet import TelnetTransport

# StormMUD imports
#from smConfig import GameConfig,
import smConfig, smPlayer, smLog
from smDefines import INFO
from smMaps import WORLD, Rooms, Doors, Map




class SonzoFactory(ServerFactory):
    """SonzoFactory is the Twisted network server for the game"""

    def __init__(self):
        """Initialize any attributes the server needs"""

        pass



#============================================
# Start Game
#============================================
def main():
    """main()"""

    #try:
    smConfig.GameConfig = smConfig.Config()
    smLog.logger = smLog.smLogger()
    LoadAMap()
    
    #Create server factory
    factory = SonzoFactory()

    factory.protocol = lambda: TelnetTransport(smPlayer.Player)
    reactor.listenTCP(smConfig.GameConfig.port, factory)
    smLog.logger.Logit(INFO, "Starting StormMUD Version: NoDamnVersion -  A SonzoSoft Product.")
    reactor.run()


# Just a small 2 room test map
def LoadAMap():
    # Create test map
    WORLD['MAIN'] = Map()
    # create a single door two split the two rooms
    WORLD['MAIN'].doors['MAIN|1'] = Doors()
    WORLD['MAIN'].doors['MAIN|2'] = Doors()
    WORLD['MAIN'].rooms['MAIN|1'] = Rooms()
    WORLD['MAIN'].rooms['MAIN|2'] = Rooms()
    
    WORLD['MAIN'].doors['MAIN|1'].DoorID = "MAIN|1"
    WORLD['MAIN'].doors['MAIN|1'].DoorType = 0
    WORLD['MAIN'].doors['MAIN|1'].DoorText = ""
    WORLD['MAIN'].doors['MAIN|1'].passable = True
    WORLD['MAIN'].doors['MAIN|1'].IsOpen = True
    WORLD['MAIN'].doors['MAIN|1'].operable = False
    WORLD['MAIN'].doors['MAIN|1'].transparent = True
    WORLD['MAIN'].doors['MAIN|1'].hidden = False
    WORLD['MAIN'].doors['MAIN|1'].locked = False
    WORLD['MAIN'].doors['MAIN|1'].IsLocked = False
    WORLD['MAIN'].doors['MAIN|1'].LockDifficulty = 0
    WORLD['MAIN'].doors['MAIN|1'].BashDifficulty = 0
    WORLD['MAIN'].doors['MAIN|1'].trap = False
    WORLD['MAIN'].doors['MAIN|1'].IsTrapped = False
    WORLD['MAIN'].doors['MAIN|1'].ExitRoom = { 'MAIN|1': 'MAIN|2', 'MAIN|2': 'MAIN|1' }
    
    WORLD['MAIN'].doors['MAIN|2'].DoorID = "MAIN|2"
    WORLD['MAIN'].doors['MAIN|2'].DoorType = 2
    WORLD['MAIN'].doors['MAIN|2'].DoorText = "gate"
    WORLD['MAIN'].doors['MAIN|2'].passable = False
    WORLD['MAIN'].doors['MAIN|2'].IsOpen = False
    WORLD['MAIN'].doors['MAIN|2'].operable = False
    WORLD['MAIN'].doors['MAIN|2'].transparent = False
    WORLD['MAIN'].doors['MAIN|2'].hidden = False
    WORLD['MAIN'].doors['MAIN|2'].locked = False
    WORLD['MAIN'].doors['MAIN|2'].IsLocked = False
    WORLD['MAIN'].doors['MAIN|2'].LockDifficulty = 0
    WORLD['MAIN'].doors['MAIN|2'].BashDifficulty = 0
    WORLD['MAIN'].doors['MAIN|2'].trap = False
    WORLD['MAIN'].doors['MAIN|2'].IsTrapped = False
    WORLD['MAIN'].doors['MAIN|2'].ExitRoom = { 'MAIN|1': 'MAIN|2', 'MAIN|2': 'MAIN|1' }
    
    # Create first room
    
    WORLD['MAIN'].rooms['MAIN|1'].RoomID = 'MAIN|1'
    WORLD['MAIN'].rooms['MAIN|1'].RoomType = [0]
    WORLD['MAIN'].rooms['MAIN|1'].name = "Test Room 1"
    WORLD['MAIN'].rooms['MAIN|1'].desc1 = "Room 1 description"
    WORLD['MAIN'].rooms['MAIN|1'].desc2 = ""
    WORLD['MAIN'].rooms['MAIN|1'].desc3 = ""
    WORLD['MAIN'].rooms['MAIN|1'].desc4 = ""
    WORLD['MAIN'].rooms['MAIN|1'].desc5 = ""
    WORLD['MAIN'].rooms['MAIN|1'].LightLevel = 1
    WORLD['MAIN'].rooms['MAIN|1'].doors = { 1: WORLD['MAIN'].doors['MAIN|1'], 5: WORLD['MAIN'].doors['MAIN|2'] }
    WORLD['MAIN'].rooms['MAIN|1'].PlayersInRoom = {}
    WORLD['MAIN'].rooms['MAIN|1'].ItemsOnGround = {}
    # Create second room
    
    WORLD['MAIN'].rooms['MAIN|2'].RoomID = 'MAIN|2'
    WORLD['MAIN'].rooms['MAIN|2'].RoomType = [0]
    WORLD['MAIN'].rooms['MAIN|2'].name = "Test Room 2"
    WORLD['MAIN'].rooms['MAIN|2'].desc1 = "Room 2 description"
    WORLD['MAIN'].rooms['MAIN|2'].desc2 = ""
    WORLD['MAIN'].rooms['MAIN|2'].desc3 = ""
    WORLD['MAIN'].rooms['MAIN|2'].desc4 = ""
    WORLD['MAIN'].rooms['MAIN|2'].desc5 = ""
    WORLD['MAIN'].rooms['MAIN|2'].LightLevel = 1
    WORLD['MAIN'].rooms['MAIN|2'].doors = { 1: WORLD['MAIN'].doors['MAIN|2'], 5: WORLD['MAIN'].doors['MAIN|1'] }
    WORLD['MAIN'].rooms['MAIN|2'].PlayersInRoom = {}
    WORLD['MAIN'].rooms['MAIN|2'].ItemsOnGround = {}
    
    
if __name__ == '__main__':
    """If main, start main()."""

    main()