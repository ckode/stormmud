#  Copyright (C) 2009, 2010 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


class Race:
    """The Race class"""
    
    def __init__(self):
        """Race -> __init__(): Initialize race class object"""
        
        self.raceid               = ""
        self.name                 = ""
        self.helpText             = ""
        
        
        # Player Stats
        self.StartingHPs          = 0
        self.HP_per_Level         = 0
        self.mana                 = 0
        self.MaxMana              = 0
        self.offense              = 0
        self.defense              = 0
        self.strength             = 0
        self.agility              = 0
        self.intellect            = 0
        self.wisdom               = 0
        self.consitution          = 0
        self.charisma             = 0
        self.AttackSpeed          = 0
        self.preception           = 0
        self.stealth              = 0
        self.SpellCasting         = 0
        self.MagicRes             = 0
        self.SavingsThrow         = 0
        self.ArmorClass           = 0
        self.experience           = 0
        self.vision               = 0
        self.hunger               = 0
        self.ResistHot            = 0
        self.ResistCold           = 0
        self.ResistAir            = 0
        self.ResistLightning      = 0
        self.ResistEarth          = 0
        self.ResistPsionics       = 0
        self.traps                = 0
        self.tracking             = 0
        self.PlayerClass          = 0
        self.PlayerRace           = 0
        self.AttackSpeed          = 0
        self.CriticalChance       = 0
        self.BackstabBonus        = 0
        self.theivery             = 0
        self.picklocks            = 0
        self.forage               = 0
        self.alchemy              = 0
        self.cooking              = 0
        self.forging              = 0
        self.leathermaking        = 0
        self.woodworking          = 0
        self.encumberance         = 0

                     
        

        
        
class Class:
    """The Race class"""
    
    def __init__(self):
        """Cace -> __init__(): Initialize race Class object"""
        
        self.classid               = ""
        self.name                  = ""
        self.helpText              = ""